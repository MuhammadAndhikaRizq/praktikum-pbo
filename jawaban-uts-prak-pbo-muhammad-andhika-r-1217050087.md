# No 1
mendemonstrasikan penyelesaian masalah dengan pendekatan matematika dan algoritma.
pada kasus ini, saya membuat sebuah kondisi yang dimana mengkalikan sebuah arah dengan nilai doronganya kemudian diubah kedalam satuan detik. seperti yang tertera pada screen record dibawah ini
![](https://gitlab.com/MuhammadAndhikaRizq/praktikum-pbo/-/raw/main/screen-record-file/no1.gif)


# No 2
masalah dalam kasus game yang saya kerjakan pada progress ini berupa, movement, obstacle, scene changer.
![](https://gitlab.com/MuhammadAndhikaRizq/praktikum-pbo/-/raw/main/screen-record-file/no2.gif)


# No 3
Encapsulation (Pembungkusan)
Pilar pertama dalam OOP adalah encapsulation, yaitu kemampuan untuk mengumpulkan data dan fungsi yang berkaitan ke dalam satu unit yang disebut objek. Objek ini kemudian dapat diatur untuk membatasi akses ke data dan fungsi yang terdapat di dalamnya, sehingga informasi yang dimiliki oleh objek dapat diproteksi dari akses yang tidak sah.

Inheritance (Pewarisan)
Pilar kedua dalam OOP adalah inheritance, yaitu kemampuan untuk membuat kelas baru dari kelas yang sudah ada. Konsep ini memungkinkan kelas baru untuk mewarisi atribut dan metode dari kelas yang sudah ada. Sehingga pengembangan kode menjadi lebih mudah dan efisien karena kode yang sama tidak perlu ditulis ulang di berbagai kelas yang berbeda.

Polymorphism (Polimorfisme)
Pilar ketiga dalam OOP adalah polymorphism, yaitu kemampuan objek untuk mengambil banyak bentuk yang berbeda. Dalam bahasa pemrograman, ini biasanya dilakukan melalui penggunaan metode atau fungsi yang sama, tetapi berperilaku berbeda tergantung pada kelas objek yang digunakan. Polymorphism membantu meningkatkan fleksibilitas dan kegunaan kode, serta mempermudah pengembangan aplikasi.

Abstraction (Abstraksi)
Pilar keempat dalam OOP adalah abstraction, yaitu kemampuan untuk menyembunyikan detail implementasi yang tidak relevan atau tidak perlu diketahui oleh pengguna. Konsep ini membantu mengurangi kompleksitas dan meningkatkan keterbacaan kode, karena hanya detail penting dan fungsionalitas yang diungkapkan ke pengguna.


# No 4
Encapsulation
berikut adalah code dari pengimplementasian dari encapsulation pada projek yang saya buat
![](https://gitlab.com/MuhammadAndhikaRizq/praktikum-pbo/-/raw/main/screen-record-file/no4.gif)

# No 5
Abstraction
berikut adalah code dari pengimplementasian dari Abstraction pada projek yang saya buat
![](https://gitlab.com/MuhammadAndhikaRizq/praktikum-pbo/-/raw/main/screen-record-file/no5.gif)


# No 6
Inheritance & polymorphism
berikut adalah code dari pengimplementasian dari Inheritance & Polymorphism pada projek yang saya buat
![](https://gitlab.com/MuhammadAndhikaRizq/praktikum-pbo/-/raw/main/screen-record-file/no6.gif)

# No 7
Use Case Diagram
![](https://gitlab.com/MuhammadAndhikaRizq/praktikum-pbo/-/raw/main/screen-record-file/UseCase.gif)


# No 8
Class diagram dan use case table
![](https://gitlab.com/MuhammadAndhikaRizq/praktikum-pbo/-/raw/main/screen-record-file/ClassDiagram.gif)
![](https://gitlab.com/MuhammadAndhikaRizq/praktikum-pbo/-/raw/main/screen-record-file/Table.gif)

# No 9
Link youtube
https://youtu.be/ZidJbZd27GA

# No 10
Tampilan Ui&Ux. hasil record 
![](https://gitlab.com/MuhammadAndhikaRizq/praktikum-pbo/-/raw/main/screen-record-file/gifapk.gif)



