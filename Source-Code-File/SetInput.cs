using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetInput 
{
    private float mainThrust;

    public SetInput(float mainThrust)
    {
        this.mainThrust = mainThrust;
    }

    // Encapsulates setter getter
    public virtual float MainTrhust
    {
        get {return mainThrust; }
    }
    
}
