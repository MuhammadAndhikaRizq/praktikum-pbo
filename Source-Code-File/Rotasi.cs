using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Inheritance pada c# menggunakan keyword :
public class Rotasi : SetInput
{
    private float mainRotasi;
    
    // Keyword constructor pada child class adalah base
    public Rotasi(float mainThrust, float mainRotasi) : base(mainThrust)
    {
        this.mainRotasi = mainRotasi;
    }

    // Polymorphism dinamis dengan mengoverride method dari class parents
    public override float MainTrhust
    {
         get {return mainRotasi; }
    }
}
