using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    // Atribut
    [SerializeField] float dorongan;
    [SerializeField] float rotasi;
    
    Rigidbody rb;
    AudioSource audioSource;


    // Object
    SetInput input =  new SetInput(1000f);
    Rotasi rotate = new Rotasi(1000f, 100f);


    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        ProcessInput();
        ProcessRotation();
    }


    // Method
    public void ProcessInput()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            // mengisi parameter method AddRelativeForce berdasarkan struktur Vector 3(x, y, z)
            // nilai dari dorong merefer pada value method Mainthrust di class SetInput
            // time.deltatime mengubah satuan per frame menjadi detik, agar kecepatannya menyesuaikan dengan detik karena setiap 30 fps = 1 detik/30 frame 
            dorongan = input.MainTrhust;
            rb.AddRelativeForce(Vector3.up * dorongan * Time.deltaTime);
            if(!audioSource.isPlaying)
            {
                audioSource.Play();
            }
        }else{
            audioSource.Stop();
        }

    }

    public void ProcessRotation()
    {
        rotasi = rotate.MainTrhust;

        if (Input.GetKey(KeyCode.A))
        {
            ApplyRotation(rotasi);
        }

        else if (Input.GetKey(KeyCode.D))
        {
            ApplyRotation(-rotasi);
        }
    }

    public void ApplyRotation(float rotationFrame)
    {
        rb.freezeRotation = true; // Memberikan efek freeze pada rotasi 
        transform.Rotate(Vector3.forward * rotationFrame * Time.deltaTime);
        rb.freezeRotation = false; // Membatalkan efek freeze dan terjadi hukum fisika
    }
    
}
