using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Scene 
{
    public abstract void LoadNextLevel();
    public abstract void ReloadLevel();
}
