using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CollisonHandler : MonoBehaviour
{

    SceneChanger scene = new SceneChanger();

    void OnCollisionEnter(Collision other) {
        switch(other.gameObject.tag)
        {
            case "Friendly":
                Debug.Log("Ini adah start awal");
                break;
            case "Finish":
                scene.LoadNextLevel();
                break;
            default:
                scene.ReloadLevel();
                break;
        }    
    }
}
