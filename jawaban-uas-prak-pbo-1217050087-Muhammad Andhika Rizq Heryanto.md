# No 1
Ini adalah hasil UseCase dari produk yang dibuat
![](https://gitlab.com/MuhammadAndhikaRizq/praktikum-pbo/-/raw/main/screen-record-file/UseCase.gif)

# No 2
Class diagram dari produk yang dibuat
![](https://gitlab.com/MuhammadAndhikaRizq/praktikum-pbo/-/raw/main/screen-record-file/ClassDiagram.gif)
![](https://gitlab.com/MuhammadAndhikaRizq/praktikum-pbo/-/raw/main/screen-record-file/Table.gif)

# No 3
Penarapan poin SOLID design patterns
Single Responsibility Principle (SRP) dan Liskov Subtittuion Principle(LSP)
saya menerapkan Liskov Subtittuion Principle(LSP)
Dalam prgram saya, kelas RotateInput mewarisi kelas SetInput dan mengganti perilaku method startCondition() dan StopCondition(). Meskipun demikian, kelas turunan tetap dapat digunakan sebagai pengganti kelas induk dalam semua konteks tanpa mengganggu fungsionalitas yang diharapkan.
![](https://gitlab.com/MuhammadAndhikaRizq/praktikum-pbo/-/raw/main/screen-record-file/lsp.gif)
saya menerapkan Responsibility Principle (SRP)
contohnya di class buttonya yang memang fokus untuk menangani perpindahan scene
![](https://gitlab.com/MuhammadAndhikaRizq/praktikum-pbo/-/raw/main/screen-record-file/srp.gif)

# No 4
Penarapan design patterns
saya menerapkan factroy method dan singleton pada project yang dibuat.
factroy method
![](https://gitlab.com/MuhammadAndhikaRizq/praktikum-pbo/-/raw/main/screen-record-file/factorymethod.gif)
singleton
![](https://gitlab.com/MuhammadAndhikaRizq/praktikum-pbo/-/raw/main/screen-record-file/singleton.gif)

# No 5
Konektivitas ke database dengan firebase
![](https://gitlab.com/MuhammadAndhikaRizq/praktikum-pbo/-/raw/main/screen-record-file/db.gif)

# No 6
Web service dan CRUD
karena disini saya menggunakan sdk firebase jadi untuk restful apinya sudah tertanam secara default pada package/libaray yang dipasang.
![](https://gitlab.com/MuhammadAndhikaRizq/praktikum-pbo/-/raw/main/screen-record-file/firebase.gif)
upload, load, edit data
![](https://gitlab.com/MuhammadAndhikaRizq/praktikum-pbo/-/raw/main/screen-record-file/crud.gif)

# No 7
gui pada unity
![](https://gitlab.com/MuhammadAndhikaRizq/praktikum-pbo/-/raw/main/screen-record-file/gui.gif)
pada kodingan tersebut terlihat jelas bahwa ada beberapa tipe data seperti particle system dan audiosource yang merupakan bagian dari tampilan pada unity

# No 8
connection gui c# dan unity
![](https://gitlab.com/MuhammadAndhikaRizq/praktikum-pbo/-/raw/main/screen-record-file/connectgui.gif)

# No 9
Link video demonstrasi youtube
https://youtu.be/ATFszdT-KIA

# No 10
tidak sempat menambhkan machine learning
